These tools automatically apply operations to target repos.

# automated-repo-history

This script adds, commits, and pushes changes for a target repo if the changes are old. This is useful in cases where important production files are modified anonymously without version control. If files are modified in this way, at least this script creates an automated history. Alternatively, people can manually commit changes to the same repo to give a more detailed explanation of their changes.

### Safety

* This tool never modifies files outside the `.git` folder it targets. 
* Performs no actions unless the target repo has a remote called `automated-repo-history`.
* There is a test suite.

### CLI

For details on usage run with the `-h` option:

```bash
automated-repo-history -h
```

### Setup

As a precaution, this tool only runs on git repos that have a remote called `automated-repo-history`. Here is a setup example:

```bash
cd ~smco500/.config/cmoi
git remote add automated-repo-history git@gitlab.science.gc.ca:CMOI/config-smco500-homeu1.git
```

This script is designed to be run regularly, even every minute. Most of the time, no old changes are detected and no actions are performed. So you may also want to add the script to a cron or hcron.

# run-tests

This script runs all automated tests on all scripts.

```bash
run-tests
```

# See Also

For more information about the origin of this script, tracking configuration repos:

https://gitlab.science.gc.ca/CMOI/cmoi-proposals/issues/32
