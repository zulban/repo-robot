import os
import os.path
import datetime
import subprocess

TEST_ROOT_FOLDER="tests/mock_files/"
TEST_HOME=TEST_ROOT_FOLDER+"home/"

ARH_SCRIPT=os.path.dirname(os.path.abspath(__file__))+"/../automated-repo-history"
TMP_REPO=os.environ["HOME"]+"/tmp/.unit-tests-arh"
FILE1=TMP_REPO+"/file1"
FILE2=TMP_REPO+"/file2"
FILE3=TMP_REPO+"/file3"
GIT_REMOTE="automated-repo-history"
FIRST_COMMIT_MESSAGE="first commit"

from utilities.generic import get_offset_datestamp

OLD_DATESTAMP=get_offset_datestamp(hours=2)