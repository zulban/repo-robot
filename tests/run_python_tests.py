#!/usr/bin/python3

"Run all tests for all scripts."

import unittest
import sys
import os
from utilities import *

tests = unittest.TestLoader().discover(".", pattern ='test_*.py')
runner = unittest.TextTestRunner(sys.stdout, verbosity=2)
runner.run(tests)