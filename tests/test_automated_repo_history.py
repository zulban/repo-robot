import datetime
import os
import os.path
import subprocess
import time

from utilities import *
from constants import *

class TestAutomatedRepoHistory(ZTest):
    
    def test_unmodified_tracked_files(self):
        setup_mock_repo(create_file1=True,
                        create_file2=True)
        before=get_commit_count()
        run_arh_script()
        after=get_commit_count()
        self.assertEqual(before,1)
        self.assertEqual(before,after)
        
    def test_new_modified_tracked_files(self):
        setup_mock_repo(modify_file1=True)
        before=get_commit_count()
        run_arh_script()
        after=get_commit_count()
        self.assertEqual(before,1)
        self.assertEqual(before,after)
        
    def test_old_new_modified_files(self):
        "two modified files, but only one is old"
        setup_mock_repo(is_file2_old=True,
                        modify_file1=True,
                        modify_file2=True)
        before=get_commit_count()
        run_arh_script()
        after=get_commit_count()
        self.assertEqual(before,1)
        self.assertEqual(before,after)
        
    def test_new_untracked_file(self):
        setup_mock_repo(create_file3=True,
                        is_file3_old=False)
        before=get_commit_count()
        run_arh_script()
        after=get_commit_count()
        self.assertEqual(before,after)
                
    def test_old_untracked_new_modified(self):
        "old untracked file, new modified tracked file"
        setup_mock_repo(modify_file2=True,
                        create_file3=True,
                        is_file2_old=False,
                        is_file3_old=True)
        before=get_commit_count()
        output=run_arh_script()
        after=get_commit_count()
        self.assertEqual(before,after,msg=output)
        
    def test_old_untracked_new_unmodified_tracked(self):
        "old untracked file and new unmodified tracked file"
        setup_mock_repo(modify_file2=True,
                        create_file3=True,
                        is_file2_old=False,
                        is_file3_old=True)
        before=get_commit_count()
        output=run_arh_script()
        after=get_commit_count()
        self.assertEqual(before,after,msg=output)
        
    def test_unmodified_old(self):
        setup_mock_repo(is_file2_old=True)
        before=get_commit_count()
        output=run_arh_script()
        after=get_commit_count()
        message="output =\n"+output
        self.assertEqual(before,after,msg=message)
        
    def test_deleted_tracked_file(self):
        setup_mock_repo(delete_file1_after_commit=True)
        before=get_commit_count()
        run_arh_script()
        after=get_commit_count()
        self.assertEqual(before,1)
        self.assertEqual(before+1,after)
        
    def test_old_modified_deleted_tracked(self):
        "old modified tracked file and a tracked deleted file"
        setup_mock_repo(delete_file1_after_commit=True,
                        modify_file2=True,
                        is_file2_old=True)
        before=get_commit_count()
        run_arh_script()
        after=get_commit_count()
        self.assertEqual(before,1)
        self.assertEqual(before+1,after)
        
    def test_new_modified_tracked_deleted(self):
        "new modified tracked file and a tracked deleted file"
        setup_mock_repo(delete_file1_after_commit=True,
                        modify_file2=True,
                        is_file2_old=False)
        before=get_commit_count()
        run_arh_script()
        after=get_commit_count()
        self.assertEqual(before,1)
        self.assertEqual(before,after)
        
    def test_dry_run(self):
        "script would commit, but does not because dry run"
        setup_mock_repo(is_file2_old=True,
                        modify_file2=True)
        before=get_test_repo_checksum()
        output=run_arh_script(dry_run=True)
        after=get_test_repo_checksum()
        
        "no files were changed, including .git files"
        self.assertEqual(before,after,msg=output)
        
    def test_old_modified_file(self):
        setup_mock_repo(create_file3=True,
                        is_file3_old=True)
        before=get_commit_count()
        output=run_arh_script()
        after=get_commit_count()
        self.assertEqual(before+1,after,msg=output)
        
    def test_old_untracked_old_modified(self):
        "old untracked file and old tracked modified file"
        setup_mock_repo(modify_file2=True,
                        create_file3=True,
                        is_file2_old=True,
                        is_file3_old=True)
        before=get_commit_count()
        output=run_arh_script()
        after=get_commit_count()
        self.assertEqual(before+1,after,msg=output)
        
    def test_moved_new_tracked_file(self):
        "move a tracked file that is too new to commit"
        
        moved_filename=FILE2+"-moved"
        setup_mock_repo()
        cmd="mv %s %s"%(FILE2,moved_filename)
        safe_check_output(cmd)
        run_arh_script()
        self.assertEqual(get_commit_count(),1)

    def test_move_old_modified_tracked(self):        
        moved_filename=FILE2+"-moved"
        setup_mock_repo(create_file2=True)
        cmd="mv %s %s"%(FILE2,moved_filename)
        safe_check_output(cmd)
        
        """
        you can easily change mtime but not ctime of a time (change time)
        so just wait a second after moving
        """
        time.sleep(1.1)
        
        run_arh_script(max_age=1)
        self.assertEqual(get_commit_count(),2)
        
    def test_delete_tracked_file(self):
        setup_mock_repo(delete_file1_after_commit=True)
        before=get_commit_count()
        output=run_arh_script()
        after=get_commit_count()
        self.assertEqual(before+1,after,msg=output)
    
    def test_file_count_in_history(self):
        setup_mock_repo(create_7_old_files=True)
        output=run_arh_script()
        commit_message=get_latest_commit_message()
        message="output =\n"+output+"\ncommit message =\n"+commit_message
        self.assertIn("7",commit_message,msg=message)
        
        "move tracked file, delete tracked file, untracked file"
        setup_mock_repo(create_file1=True,
                    create_file2=True,
                    create_file3=True,
                    modify_file2=True,
                    delete_file1_after_commit=True)
        cmd="mv %s %s"%(FILE2,FILE2+"-moved")
        safe_check_output(cmd)
        time.sleep(1.1)
        before=get_commit_count()
        output=run_arh_script(max_age=1)
        after=get_commit_count()
        self.assertEqual(before+1,after,msg=output)
        commit_message=get_latest_commit_message()
        self.assertIn("3", commit_message)
    
    def test_max_age_option(self):
        "file modified a day ago, but do not commit because of max age two days"
        setup_mock_repo(is_file2_old=True,
                        modify_file2=True)                
        before1=get_commit_count()
        before2=get_not_staged_count()
        two_days=60*60*48
        run_arh_script(max_age=two_days)
        after1=get_commit_count()        
        after2=get_not_staged_count()
        
        results=(before1,before2,after1,after2)
        for result in results:
            self.assertEqual(result,1,msg=", ".join(str(r) for r in results))

    def test_max_untracked_size_option(self):
        "file modified a day ago, but do not commit because it is too large"
        setup_mock_repo(is_file2_old=True,
                        is_file2_big=True)                
        before1=get_commit_count()
        before2=get_not_staged_count()
        run_arh_script(max_untracked_size=50)
        after1=get_commit_count()        
        after2=get_not_staged_count()
        
        results=(before1,before2,after1,after2)
        for result in results:
            self.assertEqual(result,1,msg=", ".join(str(r) for r in results))
