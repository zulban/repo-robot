from utilities import *
from constants import *

class TestUtilities(ZTest):
        
    def test_repo_stats(self):
        setup_mock_repo(modify_file1=False,
                        modify_file2=False,
                        create_file3=False,
                        verbose=False)
        self.assertIn(FIRST_COMMIT_MESSAGE,get_latest_commit_message())
        self.assertEqual(get_not_staged_count(),0,msg=str(get_not_staged()))
        self.assertEqual(get_commit_count(),1)
        
        setup_mock_repo(modify_file1=True,
                        modify_file2=True,
                        create_file3=True,
                        verbose=False)
        self.assertEqual(get_not_staged_count(),2)
        