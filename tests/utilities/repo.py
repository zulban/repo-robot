import datetime
from constants import *
from utilities import *

def run_arh_script(max_age="",
                   dry_run="",
                   max_untracked_size="",
                   mtime_ctime=True,
                   verbose=False):
    if max_age:
        "must be integer"
        int(max_age)
        max_age=" --max-age=%s "%str(max_age).strip()
    if dry_run:
        dry_run=" --dry-run "
    if mtime_ctime:
        mtime_ctime=" --mtime-ctime "
    if max_untracked_size:
        max_untracked_size=" --max-untracked-age=%s "%max_untracked_size
    cmd=ARH_SCRIPT+" "+TMP_REPO+" --no-push "+max_age+dry_run+max_untracked_size+mtime_ctime
    if verbose:
        print(cmd)
    return safe_check_output(cmd)

def get_test_repo_checksum(repo_path=""):
    "returns a single checksum that is a checksum of all file checksums in the test repo"
    if not repo_path:
        repo_path=TMP_REPO
    cmd="find %s -type f -exec md5sum {} \; | md5sum"%repo_path
    return safe_check_output(cmd)

def get_not_staged():
    "returns the tracked files with uncommited modifications"
    cmd="cd %s ; git ls-files -m"%TMP_REPO
    output=safe_check_output(cmd.strip())
    result=output.strip().split("\n")
    return [i.strip() for i in result if i.strip()]

def get_latest_commit_message():
    cmd="cd "+TMP_REPO+" ; git log -1 --pretty=%B"
    return safe_check_output(cmd.strip())

def get_not_staged_count():
    return len(get_not_staged())

def get_commit_count():
    cmd="cd %s ; git rev-list --count HEAD"%TMP_REPO
    return int(safe_check_output(cmd.strip()))

def setup_mock_repo(create_file1=False,
                    create_file2=False,
                    create_file3=False,
                    create_7_old_files=False,
                    is_file2_old=False,
                    is_file3_old=False,
                    is_file2_big=False,
                    modify_file1=False,
                    modify_file2=False,
                    delete_file1_after_commit=False,
                    verbose=False,
                    repo_path=""):
    """
    file creation flags are set to true if those files have old=true or modified=true
    
    file1 is always new
    file2 and file3 can be two hours old
    file3 is untracked
    file2 can be 68KB
    """
    if not repo_path:
        repo_path=TMP_REPO
        
    create_file1=create_file1 or modify_file1 or delete_file1_after_commit
    create_file2=create_file2 or is_file2_old or is_file2_big or modify_file2
    create_file3=create_file3 or is_file3_old
    
    cmd="""rm -rf {tmp_repo}
mkdir -p {tmp_repo}
cd {tmp_repo}
git init
touch always-file
git add always-file"""

    if create_file1:
        cmd+="\necho 1 > {file1}\ngit add {file1}"
    if create_file2:
        cmd+="\necho 2 > {file2}\ngit add {file2}"
        
    cmd+="""
git commit -m "{first_commit_message}"
git remote add {git_remote} placeholder-value-for-unit-tests
pwd
ls"""

    if modify_file1:
        cmd+="\necho 11 > {file1}"
    if modify_file2:
        cmd+="\necho 22 > {file2}"
    if is_file2_big:
        cmd="\ndd if=/dev/zero of={file2} count=64 bs=1024"
    if create_file3:
        cmd+="\necho 3 > {file3}"
        
    if create_7_old_files:
        for i in range(1,8):
            filename="file-%s-of-7"%i
            cmd+="\necho %s >> %s && touch -m -t {old_datestamp} %s"%(i,filename,filename)
    
    if is_file2_old:
        cmd+="\ntouch -m -t {old_datestamp} {file2}"
    if is_file3_old:
        cmd+="\ntouch -m -t {old_datestamp} {file3}"
        
    if delete_file1_after_commit:
        cmd+="\nrm -f {file1}"
            
    kwargs={"tmp_repo":repo_path,
            "file1":FILE1,
            "file2":FILE2,
            "file3":FILE3,
            "old_datestamp":OLD_DATESTAMP,
            "git_remote":GIT_REMOTE,
            "first_commit_message":FIRST_COMMIT_MESSAGE}
    cmd=cmd.format(**kwargs)
    cmd=cmd.replace("\n"," && ")
    if verbose:
        print("setup_mock_repo:\n    "+cmd)
    return safe_check_output(cmd)
