
import unittest, time
from utilities.generic import show_duration
from utilities.colors import *

class ZTest(unittest.TestCase):

    """This is a generic abstract class that all tests should inherit from."""

    @classmethod
    def setUpClass(cls):
        cls.class_start_time = time.time()

    def setUp(self):
        self.start_time = time.time()

    @classmethod
    def tearDownClass(cls):
        label = "Class " + str(cls).split(".")[-1][:-2]
        show_duration(label, cls.class_start_time, print_function=print_orange)

    def tearDown(self):
        label = ".".join(self.id().split(".")[2:])
        show_duration(label, self.start_time)
